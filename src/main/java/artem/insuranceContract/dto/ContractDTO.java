package artem.insuranceContract.dto;

import artem.insuranceContract.model.User;
import lombok.Data;

import javax.persistence.ManyToOne;
import java.math.BigDecimal;
import java.time.LocalDate;

@Data
public class ContractDTO {

    private LocalDate startingDate;
    private LocalDate expirationDate;
    private BigDecimal price;
    private BigDecimal coverageSum;
    private UserDTO userDTO;
}
