package artem.insuranceContract.dto;

import lombok.Data;

import java.time.LocalDate;

@Data
public class UserDTO {

    private String firstName;
    private String lastName;
    private LocalDate birthDate;
    private String phoneNumber;
}
