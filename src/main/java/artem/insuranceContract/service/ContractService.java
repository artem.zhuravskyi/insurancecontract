package artem.insuranceContract.service;

import artem.insuranceContract.dto.ContractDTO;
import artem.insuranceContract.dto.UserDTO;
import artem.insuranceContract.model.Contract;
import artem.insuranceContract.model.User;
import artem.insuranceContract.repository.ContractRepository;
import artem.insuranceContract.repository.UserRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.time.LocalDate;
import java.util.List;

@Service
@AllArgsConstructor
public class ContractService {

    private ContractRepository contractRepository;
    private UserRepository userRepository;
    private UserService userService;

    @Transactional
    public Contract createContract(ContractDTO contractDTO) {
        User user = userService.findByPhoneNumber(contractDTO.getUserDTO().getPhoneNumber());
        if (user == null) {
            user = userService.createUser(contractDTO.getUserDTO());
        }
        Contract contract = Contract.builder()
                .price(contractDTO.getPrice())
                .coverageSum(contractDTO.getCoverageSum())
                .creationDate(LocalDate.now())
                .startingDate(contractDTO.getStartingDate())
                .expirationDate(contractDTO.getExpirationDate())
                .user(user)
                .build();
        contractRepository.save(contract);
        return contract;
    }

    public List<Contract> getContracts() {
        return contractRepository.findAll();
    }
}
