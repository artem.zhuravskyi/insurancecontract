package artem.insuranceContract.service;

import artem.insuranceContract.dto.UserDTO;
import artem.insuranceContract.model.User;
import artem.insuranceContract.repository.UserRepository;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UserService {

    private UserRepository userRepository;

    public User createUser(UserDTO userDTO) {
        User user = User.builder()
                .firstName(userDTO.getFirstName())
                .lastName(userDTO.getLastName())
                .birthDate(userDTO.getBirthDate())
                .phoneNumber(userDTO.getPhoneNumber())
                .build();
        userRepository.save(user);
        return user;
    }

    public User findByPhoneNumber(String phoneNumber) {
        return userRepository.findUserByPhoneNumber(phoneNumber).orElse(null);
    }

}
