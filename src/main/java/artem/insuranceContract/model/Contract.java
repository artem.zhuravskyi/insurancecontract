package artem.insuranceContract.model;

import lombok.*;

import javax.persistence.*;
import java.math.BigDecimal;
import java.time.LocalDate;

@Table(name = "contracts")
@Entity
@Getter
@Setter
@Builder

@AllArgsConstructor
@NoArgsConstructor
public class Contract {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;
    private LocalDate creationDate;
    private LocalDate startingDate;
    private LocalDate expirationDate;
    private BigDecimal price;
    private BigDecimal coverageSum;

    @ManyToOne
    private User user;

}
