package artem.insuranceContract.controller;

import artem.insuranceContract.dto.ContractDTO;
import artem.insuranceContract.dto.UserDTO;
import artem.insuranceContract.model.Contract;
import artem.insuranceContract.service.ContractService;
import jdk.jfr.ContentType;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@AllArgsConstructor
public class ContractController {

    private ContractService contractService;

    @PostMapping("create-contract")
    public Contract createContract(ContractDTO contractDTO) {
        return contractService.createContract(contractDTO);
    }

    @RequestMapping(value = "create-contract", method = RequestMethod.GET, consumes = "application/html")
    public String createContract() {
        return "create-contract";
    }

    @GetMapping("contracts")
    public List<Contract> showContracts() {
        return contractService.getContracts();
    }

}
